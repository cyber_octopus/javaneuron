# README #

This projects implements Learning By Stimulation Avoidance in an Izhikevich Network.

The project was developed using the Eclipse IDE for java. Demos can be run by importing the code as Java project and simply pushing the run button.
We use the Jung library for visualization; visualization can be activated or deactivated by changing the value of the "draw" variable in each demo file.

The project contains many demos that can be selected in the Starter.java file.
To save files the Constants folder's "DataPath" variable must be edited to an appropriate file.

Documentation is available in the "doc" folder, by opening the index.html file in a navigator.